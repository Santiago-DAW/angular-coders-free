import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formulario-reactivo',
  templateUrl: './formulario-reactivo.component.html',
  styleUrls: ['./formulario-reactivo.component.css']
})
export class FormularioReactivoComponent {
  // Inyectamos la dependencia FormBuilder.
  constructor(private fb: FormBuilder) { }

  // formUser = new FormGroup({
  //   name: new FormControl('', Validators.required),
  //   email: new FormControl('', [Validators.required, Validators.email])
  // });

  // Definimos un grupo utilizando fb (formBuilder).
  formUser = this.fb.group({
    // Usamos un array en lugar crear una instancia con new Formcontrol() .
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]]
  });

  get name() {
    return this.formUser.get('name') as FormControl;
  }
  get email() {
    return this.formUser.get('email') as FormControl;
  }

  procesar() {
    console.log(this.formUser.value);
  }
}
